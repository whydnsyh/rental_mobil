<?php 
 
class M_mobil extends CI_Model

    {
        
    function show_mobil()
	{
		$this->db->order_by('mobil_id', 'DESC');
		$this->db->limit(10);
		$query = $this->db->get('mobil');		  
		return $query->result();
    }
    
    function insert_data($data, $table){
    $this->db->insert($table, $data);
    }
            
    function update_data($where, $data, $table){
    $this->db->where($where);
    $this->db->update($table, $data);
    }
            
    function delete_data($where, $table){
    $this->db->where($where);
    $this->db->delete($table);
    }
} 
