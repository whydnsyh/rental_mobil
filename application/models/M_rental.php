<?php
class M_rental extends CI_Model{
    function edit_data($where, $table){
        return $this->db->get_where($table,$where);
    }
    
    function get_data($table){
        return $this->db->get($table);
    }
    public function get_all()
    {
        return $this->db->get("mobil");
    }
    public function get_tr()
    {
        return $this->db->get("transaksi");
    }
    public function get_ts()
    {
        return $this->db->get("kostumer");
    }
    function insert_data($data, $table){
        $this->db->insert($table, $data);
    }
    
    function update_data($where, $data, $table){
        $this->db->where($where);
        $this->db->update($table, $data);
    }
    
    function delete_data($where, $table){
        $this->db->where($where);
        $this->db->delete($table);
    }
}