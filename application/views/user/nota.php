  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Nota Pembayaran</h1>
  </div>
  <!-- Content Row -->
  <div class="row">

  <div class="selectroom">
	<div class="container">	
	<?php 
        
        foreach($transaksi as $t){
	?>
    <tr>
                        
                        <td>Tanggal : <?php echo date('d/m/Y',strtotime($t->transaksi_tgl)); ?></td>
                        </br>
                        </br>
                        <td>Nama Penyewa : <?php echo '<i class="fas fa-user"> '.$t->kostumer_nama.'<br><i class="fas fa-car">'.$t->mobil_merk ?></td>
                        </br>
                        </br>
                        <td>Tanggal Peminjaman : <?php echo date('d/m/Y',strtotime($t->transaksi_tgl_pinjam)).'<br>'.date('d/m/Y',strtotime($t->transaksi_tgl_kembali)); ?></td>
                        </br>
                        </br>
                        <td>Tanggal Pengembalian : <?php echo 'Rp. '.number_format($t->transaksi_harga) ?></td>
                        <td><?php echo 'Rp. '.number_format($t->transaksi_denda) ?></td>
                        <td><?php echo ($t->transaksi_tgldikembalikan == '0000-00-00') ? '--' : date('d/m/Y',strtotime($t->transaksi_tgldikembalikan)); ?></td>
                        <td><?php echo 'Rp. '.number_format($t->transaksi_totaldenda) ?></td>
                        
                    </tr>
<?php } ?>
  </div>

  <!-- Content Row -->
  

  

  <!-- Content Row -->
  
  <script src="<?php echo base_url().'assets/'; ?>js/demo/chart-area-demo.js"></script>
  <!-- <script src="<?php echo base_url().'assets/'; ?>js/demo/chart-pie-demo.js"></script> -->