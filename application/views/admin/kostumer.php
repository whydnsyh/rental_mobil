<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Data Kostumer</h1>
    <a href="#" data-toggle="modal" data-target="#addModal"class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-plus fa-sm text-white-50"></i> Tambah Kostumer</a>
</div>
    
<div class="card shadow mb-4">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama</th>
                        <th>Jenis Kelamin</th>
                        <th>Username/Password</th>
                        <th>No.Handphone/No.KTP</th>
                        <th>Alamat</th>
                        <th>Email</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                $no=1;
                foreach($kostumer as $k){
                ?>
                    <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $k->kostumer_nama ?></td>
                        <td><?php echo $k->kostumer_jk ?></td>
                        <td><?php echo '<i class="fas fa-user"></i> '.$k->username.'<br><i class="fas fa-id-card"></i> '.$k->password; ?></td>
                        <td><?php echo '<i class="fas fa-mobile-alt"></i> '.$k->kostumer_hp.'<br><i class="fas fa-id-card"></i> '.$k->kostumer_ktp; ?></td>
                        <td><?php echo $k->kostumer_alamat ?></td>
                        <td><?php echo $k->kostumer_email ?></td>
                        <td>
                            <a class="btn btn-info btn-sm" href="<?php echo base_url().'admin/kostumer_edit/'.$k->kostumer_id; ?>">
                                <i class="fas fa-edit"></i> Edit
                            </a>
                            <a class="btn btn-danger btn-sm btn-hapus" onclick="return confirm('Anda yakin mau menghapus item ini ?')" href="<?php echo base_url().'admin/kostumer_hapus/'.$k->kostumer_id; ?>">
                                <i class="fas fa-trash"></i> Hapus
                            </a>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php echo form_open_multipart('admin/add_user_act')?>
                            <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-body">

                                        <div class="form-group">
                                            <label>Nama Lengkap</label>
                                            <div class="box-body pad">
                                         <input id="nama" name="nama" class="form-control" type="text" rows="1" cols="73" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Username</label>
                                            <div class="box-body pad">
                                         <input id="username" name="username" class="form-control" type="text" rows="1" cols="73" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Password</label>
                                            <div class="box-body pad">
                                         <input id="password" name="password" class="form-control" type="text" rows="1" cols="73" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Nomor KTP</label>
                                            <div class="box-body pad">
                                         <input id="no_ktp" name="no_ktp" class="form-control" type="text" rows="1" cols="73" required>
                                        </div>
                                        </br>
                                        <div class="form-group">
                                            <label>Tanggal Lahir</label>
                                            <div class="box-body pad">
                                         <input id="tgl_lahir" name="tgl_lahir" class="form-control" type="date" rows="1" cols="73" required>
                                        </div>
                                        </br>
                                        <div class="form-group">
                                            <label>Jenis Kelamin</label>
                                            <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="jk" value="Laki-laki" required="required">
                                            <label class="form-check-label">Laki-laki</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="jk" value="Perempuan" required="required">
                                            <label class="form-check-label">Perempuan</label>
                                        </div>
                                        </br>
                                        </br>
                                        <div class="form-group">
                                            <label>Email</label>
                                            <div class="box-body pad">
                                         <input id="email" name="email" class="form-control" type="email" rows="1" cols="73" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Nomor Telepon</label>
                                            <div class="box-body pad">
                                         <input id="no_tlp" name="no_tlp" class="form-control" type="text" rows="1" cols="73" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Alamat</label>
                                            <div class="box-body pad">
                                         <input id="alamat" name="alamat" class="form-control" type="textarea" rows="1" cols="73" required>
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->

                                    </div>

                                    
                                   <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" id="myModal1"></div>