<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Laporan data mobil</h1>
    <a href="<?= base_url('admin/export') ?>" class="btn btn-primary" target="_blank">Export</a>
</div>
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Laporan data transaksi</h1>
    <a href="<?= base_url('admin/exportT') ?>" class="btn btn-primary" target="_blank">Export</a>
</div> 
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Laporan data kostumer</h1>
    <a href="<?= base_url('admin/exportS') ?>" class="btn btn-primary" target="_blank">Export</a>
</div> 
<div class="card shadow mb-4"></div>
