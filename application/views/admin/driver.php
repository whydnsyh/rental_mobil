<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Data Driver</h1>
    <a href="#" data-toggle="modal" data-target="#addModal" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-plus fa-sm text-white-50"></i> Tambah Driver</a>
</div>
    
<div class="card shadow mb-4">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama Driver</th>
                        <th>Nomor KTP</th>
                        <th>Nomor Telepon</th>
                        <th>Tempat Lahir</th>
                        <th>Tanggal Lahir</th>
                        <th>Alamat</th>
                        <th>Foto</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                $no=1;
                foreach($driver as $m){
                ?>
                    <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $m->nama ?></td>
                        <td><?php echo $m->no_ktp ?></td>
                        <td><?php echo $m->no_hp ?></td>
                        <td><?php echo $m->tempat_lahir ?></td>
                        <td><?php echo $m->tanggal_lahir ?></td>
                        <td><?php echo $m->alamat ?></td>
                        <td><img  src='<?=base_url()?>upload/<?=$m->foto;?>' style="width:150px;height:150px; ">
                        </td>
                        <td>
                            <a class="btn btn-info btn-sm" href="<?php echo base_url().'admin/driver_edit/'.$m->id_driver; ?>">
                                <i class="fas fa-edit"></i> Edit
                            </a>
                            <a class="btn btn-danger btn-sm btn-hapus" onclick="return confirm('Anda yakin mau menghapus item ini ?')" href="<?php echo base_url().'admin/driver_hapus/'.$m->id_driver; ?>">
                                <i class="fas fa-trash"></i> Hapus
                            </a>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

                    <?php echo form_open_multipart('admin/add_driver_act')?>
                            <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-body">

                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Nama</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" name="nama" required="required"></div>
                                            <?php echo form_error('nama'); ?>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Nomor KTP</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" name="no_ktp" required="required"></div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Nomor Handphone</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" name="no_hp" required="required"></div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Jenis Kelamin</label>
                                            <div class="col-sm-10 pt-2">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="jk" value="Laki-laki" required="required">
                                                    <label class="form-check-label">Laki-laki</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="jk" value="Perempuan" required="required">
                                                    <label class="form-check-label">Perempuan</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Tempat Lahir</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" name="tempat_lahir" required="required"></div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Tanggal Lahir</label>
                                            <div class="col-sm-10"><input type="date" class="form-control" name="tanggal_lahir" required="required"></div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Alamat</label>
                                            <div class="col-sm-10">
                                            <textarea class="form-control" name="alamat" rows="3" required="required"></textarea>
                                            </div>
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->

                                    </div>

                                    
                                   <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" id="myModal1"></div>