<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Showroom Angga Motor 2</title>

  <!-- Bootstrap core CSS -->
  <link href="assets/assets/shop/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="assets/assets/shop/css/shop-homepage.css" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="#">Showroom Angga Motor 2</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <!-- <li class="nav-item">
            <a class="nav-link" href="#">About</a>
          </li> -->
          <!-- <li class="nav-item">
            <a class="nav-link" href="#">Services</a>
          </li> -->
          <li class="nav-item">
            <a class="nav-link" href="#" data-toggle="modal" data-target="#Modal_login" >login</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#" data-toggle="modal" data-target="#Modal_daftar" >Daftar</a>
          </li> 
        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Content -->
  <div class="container">

    <div class="row">

      <div class="col-lg-3">

        <!-- <h1 class="my-4">Shop Name</h1>
        <div class="list-group">
          <a href="#" class="list-group-item">Category 1</a>
          <a href="#" class="list-group-item">Category 2</a>
          <a href="#" class="list-group-item">Category 3</a>
        </div> -->

      </div>
      <!-- /.col-lg-3 -->
		</br>
        </br>
        </br>
        </br>
		</br>
        <div class="row">
		

		<?php

        foreach($mobil as $i):
        // $img=$i['img'];
        ?>


          <div class="col-lg-4 col-md-6 mb-4">
            <div class="card h-100">
              <a><img class="card-img-top" src='<?=base_url()?>upload/<?=$i->img;?>' alt=""></a>
              <div class="card-body">
                <h4 class="card-title" style="text-align:center">
                  <a ><?php echo $i->mobil_merk;?></a>
				</h4>
				<h4 class="card-title" style="text-align:center">
                  <a>Rp.<?php echo $i->mobil_harga;?></a>
                </h4>
                <h5 style="text-align:center"><?php echo $i->mobil_tahun;?></h5>
                <p class="card-text" style="text-align:center"><?php 
                    if($i->mobil_status == 1){ 
                        echo '<span class="badge  badge-primary">Ready</span>';
                    } elseif($i->mobil_status == 2) { 
                        echo '<span class="badge  badge-danger">Booked</span>'; 
                    } else {
                        echo '<span class="badge  badge-warning">Service</span>';
                    }
                ?></p>
              </div>
              
            </div>
        </div>
        <!-- /.row -->
		<?php endforeach;?>
      </div>
      <!-- /.col-lg-9 -->
      </div>

    </div>
    <!-- /.row -->

  </div>
  <!-- /.container -->

  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; Your Website 2020</p>
    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="assets/assets/shop/vendor/jquery/jquery.min.js"></script>
  <script src="assets/assets/shop/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>

<div class="modal fade" id="Modal_daftar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
    <form action="<?php echo base_url().'welcome/kostumer_add_act' ?>" method="post">
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
          <h4 class="modal-title">Form pendaftaran</h4>
        </div>
        <div class="modal-body">
          
        <div class="modal-body">
                  <!-- <input type="hidden" name="Nim" > -->
			<div class="form-group row">
                <label class="col-sm-2 col-form-label">Nama</label>
                <div class="col-sm-10"><input type="text" class="form-control" name="nama" required="required"></div>
                <?php echo form_error('nama'); ?>
			</div>
			<div class="form-group row">
                <label class="col-sm-2 col-form-label">Username</label>
                <div class="col-sm-10"><input type="text" class="form-control" name="username" required="required"></div>
                <?php echo form_error('nama'); ?>
			</div>
			<div class="form-group row">
                <label class="col-sm-2 col-form-label">Password</label>
                <div class="col-sm-10"><input type="password" class="form-control" name="password" required="required"></div>
                <?php echo form_error('nama'); ?>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Alamat</label>
                <div class="col-sm-10">
                <textarea class="form-control" name="alamat" rows="3" required="required"></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Email</label>
                <div class="col-sm-10"><input type="email" class="form-control" name="email" required="required"></div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Jenis Kelamin</label>
                <div class="col-sm-10 pt-2">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="jk" value="L" required="required">
                        <label class="form-check-label">Laki-laki</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="jk" value="P" required="required">
                        <label class="form-check-label">Perempuan</label>
                    </div>
                </div>
                <?php echo form_error('status'); ?>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Nomor Handphone</label>
                <div class="col-sm-10"><input type="text" class="form-control" name="hp" required="required"></div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Nomor KTP</label>
                <div class="col-sm-10"><input type="text" class="form-control" name="ktp" required="required"></div>
                <?php echo form_error('ktp'); ?>
            </div>
        </form>
                </div>
          <br>
        </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary"></i>Daftar</button>
          </div>
        </form>
    </div>
</div>    
</div>

<div class="modal fade" id="Modal_login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
    <form action="<?php echo base_url().'welcome/login' ?>" method="post">
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
          <h4 class="modal-title">Form login</h4>
        </div>
        <div class="modal-body">
          
        <div class="modal-body">
                  <!-- <input type="hidden" name="Nim" > -->
			<div class="form-group row">
                <label class="col-sm-2 col-form-label">Username</label>
                <div class="col-sm-10"><input type="text" class="form-control" name="username" required="required"></div>
                <?php echo form_error('nama'); ?>
			</div>
			<div class="form-group row">
                <label class="col-sm-2 col-form-label">Password</label>
                <div class="col-sm-10"><input type="password" class="form-control" name="password" required="required"></div>
                <?php echo form_error('nama'); ?>
            </div>
        </form>
                </div>
          <br>
        </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary"></i>Login</button>
          </div>
        </form>
    </div>
</div>    
</div>

<script>
function myFunction() {
  var x = document.getElementById("modal-tambah");
  if (x.style.display === "Sedang Dirental") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>