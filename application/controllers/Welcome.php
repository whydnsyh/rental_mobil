<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('m_mobil');
        $this->load->model('m_transaksi');
        $this->load->model('m_user');

    }
   
    public function index(){
        $data['mobil'] = $this->m_mobil->show_mobil();
        $this->load->view('index',$data);
    }
    
    function login(){
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $this->form_validation->set_rules('username','Username','trim|required');
        $this->form_validation->set_rules('password','Password','trim|required');
        if($this->form_validation->run() != false){
            $where = array(
                'username' => $username,
                'password' => md5($password)
            );
            $data = $this->m_user->edit_data($where,'kostumer');
            $d = $this->m_user->edit_data($where,'kostumer')->row();
            $cek = $data->num_rows();
            if($cek > 0){
                $session = array(
                    'id' => $d->kostumer_id,
                    'nama' => $d->kostumer_nama,
                    'status' => 'login'
                );
                $this->session->set_userdata($session);
                redirect(base_url().'user/index');
            } else {
                redirect(base_url().'welcome?pesan=gagal');
            }
        } else {
            $this->load->view('login');
        }
    }

    function kostumer_add_act(){
        $nama   = $this->input->post('nama');
        $username   = $this->input->post('username');
        $password   = $this->input->post('password');
        $alamat = $this->input->post('alamat');
        $email  = $this->input->post('email');
        $jk     = $this->input->post('jk');
        $hp     = $this->input->post('hp');
        $ktp    = $this->input->post('ktp');
        $this->form_validation->set_rules('nama','Nama','required');
        $this->form_validation->set_rules('ktp','Nomor KTP','required');
        
        if($this->form_validation->run() != false){
            $data = array(
                'kostumer_nama' => $nama,
                'username' => $username,
                'password' => md5($password),
                'kostumer_alamat' => $alamat,
                'kostumer_email' => $email,
                'kostumer_jk'   => $jk,
                'kostumer_hp'   => $hp,
                'kostumer_ktp'  => $ktp
            );
            
            $insert =$this->m_user->insert_data($data, 'kostumer');
            if ($simpan) {
				echo "<script> alert('Anda telah berhasil melakukan registrasi.'); </script><meta http-equiv=refresh content=0;url='".base_url()."'>";
			} else {
				echo "Anda gagal mendaftar";
			}
            redirect();
        } 
    }
}
