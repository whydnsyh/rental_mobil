<?php
defined('BASEPATH') OR exit('no direct script access allowed');

class User extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('m_user');
        //cek login
        if($this->session->userdata('status') != 'login'){
            redirect(base_url().'welcome?pesan=anda belum login');
        }
    }

    function index(){
        $data['transaksi'] = $this->db->query("select * from transaksi,mobil,kostumer where transaksi_mobil=mobil_id and transaksi_kostumer=kostumer_id order by transaksi_id desc limit 10")->result(); //transaksi terakhir
        $data['kostumer'] = $this->db->query("select * from kostumer order by kostumer_id desc limit 3")->result(); //menampilkan kostumer baru
        $data['mobil'] = $this->db->query("select * from mobil order by mobil_id desc limit 3")->result(); //menampilkan mobil baru
        $this->load->view('user/header');
        $this->load->view('user/index',$data);
        $this->load->view('user/footer');
    }

    function nota(){
        $data['transaksi'] = $this->m_user->get_data('transaksi')->result();
        $this->load->view('user/header');
        $this->load->view('user/nota',$data);
        $this->load->view('user/footer');
    }

    // function nota_cek(){
    //     $where = array('transaksi_id' => $id);
    //     $data['transaksi'] = $this->m_rental->edit_data($where,'transaksi')->result();
    //     $this->load->view('user/header');
    //     $this->load->view('user/nota',$data);
    //     $this->load->view('user/footer');
    // }

    function rental(){
        $w = array('mobil_status' => 1);
        $data['mobil'] = $this->m_rental->edit_data($w,'mobil')->result();
        $data['kostumer'] = $this->m_rental->get_data('kostumer')->result();
        $this->load->view('user/header');
        $this->load->view('user/rental', $data);
        $this->load->view('user/footer');
    }
    function rental_add_act(){
        $kostumer   = $this->input->post('kostumer');
        $mobil      = $this->input->post('mobil');
        $tgl_pinjam = $this->input->post('tgl_pinjam');
        $tgl_kembali= $this->input->post('tgl_kembali');
        $harga      = $this->input->post('harga');
        $denda      = $this->input->post('denda');
        
        $this->form_validation->set_rules('kostumer', 'Kostumer', 'required');
        $this->form_validation->set_rules('mobil', 'Mobil', 'required');
        $this->form_validation->set_rules('tgl_pinjam', 'Tanggal Pinjam', 'required');
        $this->form_validation->set_rules('tgl_kembali', 'Tanggal Kembali', 'required');
        $this->form_validation->set_rules('harga', 'Harga', 'required');
        $this->form_validation->set_rules('denda', 'Denda', 'required');
        
        if($this->form_validation->run() != false){
            $data = array(
                'transaksi_karyawan'    => $this->session->userdata('id'),
                'transaksi_kostumer'    => $kostumer,
                'transaksi_mobil'       => $mobil,
                'transaksi_tgl_pinjam'  => $tgl_pinjam,
                'transaksi_tgl_kembali' => $tgl_kembali,
                'transaksi_harga'       => $harga,
                'transaksi_denda'       => $denda,
                'transaksi_tgl'         => date('Y-m-d')
            );
            
            $this->m_rental->insert_data($data, 'transaksi');
            
            //update status mobil yg dipinjam
            $d = array('mobil_status' => 2);
            $w = array('mobil_id' => $mobil);
            $this->m_rental->update_data($w, $d, 'mobil');
            
            redirect(base_url().'user/nota');
        } else {
            $d = array('mobil_status' => 1);
            $data['mobil']      = $this->m_rental->edit_data($d, 'mobil')->result();
            $data['kostumer']   = $this->m_rental->get_data('kostumer')->result();
            $this->load->view('admin/header');
            $this->load->view('admin/transaksi_add', $data);
            $this->load->view('admin/footer');
        }
    }

    function logout(){
        $this->session->sess_destroy();
        redirect(base_url(''));
    }
    
    
}
?>